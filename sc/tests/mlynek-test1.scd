
// https://www.youtube.com/watch?v=3NgCFsmGQgM&t=1204s

s.boot;
BufWr
b = Buffer.alloc(s, s.sampleRate * 0.467);
b.plot;

(
y = {PlayBuf.ar(1!2, b, loop: 1)}.play;
)

y.free;
(
x = {
	var sig;
	sig = SoundIn.ar(0);
	sig = SinOsc.ar(SinOsc.ar(3).range(500, 700));
	// run - zrejme pauza v nahravani
	RecordBuf.ar(sig, b, \offset.kr(0),
		\reclev.kr(0).varlag(0.3),
		\prelev.kr(0).varlag(0.3),
		\run.kr(1),
		\loop.kr(1),
		doneAction: 2);
	sig = sig!2;

}.play;
)

(
SynthDef(\recBuf, {
	var sig, env;
	sig = SoundIn.ar(0);
	sig = SinOsc.ar(SinOsc.ar(3).range(500, 700));
	env = EnvGen.ar(Env.perc(), doneAction: Done.freeSelf);
	// run - zrejme pauza v nahravani
	RecordBuf.ar(sig, b, \offset.kr(0),
		\reclev.kr(0).varlag(0.3),
		\prelev.kr(0).varlag(0.3),
		\run.kr(1),
		\loop.kr(0),
		doneAction: Done.freeSelf);

}).add();
)
s.boot;
b.plot

a = Synth.new(\recBuf);
a.set(\reclev, 1);
s.freeAll;

//iba prehravam
x.set(\reclev, 0, \prelev, 1);
x.free;
//nahravam
x.set(\reclev, 1, \prelev, 0);


/****************************************************************************************/

s.quit;
s.freeAll;
s.boot;
s.meter;


~srcBuf = Buffer.alloc(s, s.sampleRate * 4);
~srcBuf.plot;
~srcBus = Bus.audio(s, 1);



~tarBuf = Buffer.alloc(s, s.sampleRate * 5);
~tarBuf.setn(0.5, { |i| i.linrand });

~tarBuf.free;
~tarBuf.plot;

~tarBuf.plot.refresh;
~tarBufPLOT.refresh();



//create buff with sound

(
x = {
	var sig, env, envDust;
	sig = SinOsc.ar(SinOsc.ar(10).range(400, 600));
	envDust = Dust.ar(5);
//	env = EnvGen.ar(Env.perc(0.5, 4.5, curve: 3));
	sig = sig * envDust;
	RecordBuf.ar(sig, ~srcBuf, loop: 0,  doneAction: 2);
}.play
)

(
SynthDef.new(\playSrc, {
	var sig = PlayBuf.ar(1, \buf.kr(0), loop: 1) * 0.1;
	Out.ar(\outBus.kr(0), sig);
}).add;
)

x = Synth.new(\playSrc, [\buf, ~tarBuf]);
x.free




~tarBuf = Buffer.alloc(s, s.sampleRate * 1,1);


x = Array.fill(~tarBuf.numFrames, {1.0.rand});
~tarBuf.loadCollection(x, 0);



~tarBufPLOT = ~tarBuf.plot;
~tarBufPLOT.setValue(x);
~tarBuf.free;
// -------
(
SynthDef.new(\recWithGate, {
//	var sig = In.ar(\inBus.ir(0));
	var sig = SoundIn.ar(0);
	var amp = Amplitude.ar(sig, \ampAtt.kr(0.1), \ampRel.kr(0.1));
	var isOn = amp > 0.01;

//	isOn.poll(\pollFreq.ar(5), "isOn");
	//isOn.poll(5, "isOn");
	RecordBuf.ar(sig, ~tarBuf, run: isOn ,loop: 1,  doneAction: 0);
	Out.ar(0, sig);
}).add;
)

y = Synth.new(\recWithGate, [\inBus, ~srcBus ]);

y.free;

{ Amplitude.ar(SoundIn.ar(0)); }.scope;

(
var test = 0.3;
var ison = test > 0.2;
ison.postln;
)














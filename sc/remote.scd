// remote


/*****
Run the code below from the SuperCollider IDE after you started scsynth on Bela (see -main.scd)

Make sure you have the Bela Remote SC classes are in the extensions directory of your SC installation.
Get them here: https://github.com/sensestage/bela-remote

(c) 2017: Jonathan Reus, Marije Baalman, Giulio Moro, Andrew McPherson
*****/

x.free
(
var freq = 2;
var smpDur = 1/s.sampleRate;
var ms = s.sampleRate / 1000;
x = {
	var duration, responseTrig, distance;
	var trig = Trig.ar(Impulse.ar(freq), 400 / 1000);
	var swp;
	DigitalOut.ar(0, trig);
	responseTrig = Trig.ar(DigitalIn.ar(1), 10 / 1000);
	swp = Sweep.ar(trig | responseTrig);
	swp.belaScope(0);
	duration = Timer.ar(trig | responseTrig) * 1e4; // 1e6 = to microseconds
	duration.poll;
	distance = Latch.ar(duration, responseTrig);
	MPU
	//((distance / 58) - 21).poll;

	//Trig.ar(Impulse.ar(1), 0.1).poll;


}.play;
)

(
x = {
	AnalogOut.ar(0, 1);
	AnalogIn.ar(0).poll;
}.play
)

x.free
(
// DIODA
// zapaja sa do GND + Analog in 7 6 5
// 5V je schvalne preskocene
x.free;
x = {
	var amp = 0.6;
	var phase = Saw.kr(4).range(0,1) > 0.5;
	var rgb = [0.9,0.0,0.0].reverse;
	AnalogOut.kr([5,6,7], rgb*amp*phase);
}.play
)
s.freeAll
x.free
( // connect to the already-running remote belaserver
//Server.default = s = Server("belaServer", NetAddr("bela.local", 57110));
Server.default = s = Server("belaServer", NetAddr("192.168.1.200", 57110));
s.options.maxLogins = 4; // should match the settings on the Bela
s.initTree;
s.startAliveThread;
);

// Communicate with the server
s.plotTree; // show all active nodes
s.meter
s.freeAll;  // free all nodes

(

//MIN 0.0043182373046875
//MAX 0.82591247558594

~min = 2;
~max = 0;
~bus_MILL_RAW_IN = Bus.control(s, 1);


~min.postln;
~max.postln;

)

(

var pin = 0;
SynthDef(\millSensorSynth, {
	var sig = AnalogIn.kr(\pin.ir(0));
	var scaled = sig.linlin(0.0043182373046875, 0.82591247558594, 0, 1);
	Out.kr(\rawBus.kr(0), scaled);

}).add;
~remote_MillSensorSynth = Synth(\sensorSynth, [
	\rawBus, ~bus_MILL_RAW_IN,
	\pin, analogInPin,
] );
)


(
var pin = 0;
SynthDef(\capSensorSynth, {
	var sig = DigitalIn.kr(\pin.ir(0));
	sig.poll; //TODO: scale
	Out.kr(\capBus.kr(0), sig);
}).add;
)

~remote_CapSensorSynth= Synth(\capSensorSynth, [
	\capBus, ~bus_CAP_OPENED,
	\pin, pin]
);

(

BelaScope.monitorBus(0, ~bus_MILL_RAW_IN, 1, Server.default, 'control');
//BelaScope.monitorBus(1, ~bus_MILL_EASED, 1, Server.default, 'control');
//BelaScope.monitorBus(2, ~bus_SoundSrc, 1, Server.default);

BelaScope.monitorBus(1, ~bus_MILL_RPS, 1, Server.default, 'control');


BelaScope.monitorBus(2, ~bus_PhaseRecOut, 1, Server.default,  'control');


	//BelaScope.scope(0, ~bus_MILL_EASED);

)
s.plotTree
SynthDef.new(\setMillZeroPosition, {
	var phase = In.kr(\millRaw.ir(0), 1);
	SendReply.kr(Impulse.kr(10), '/set_mill_offset', phase);
	0.0;
}).add;

x = Synth(\setMillZeroPosition, [\millRaw, ~bus_MILL_RAW_IN]);
x.free

~const_MILL_OFFSET = 0;

(
var f, g;
var path = PathName(thisProcess.nowExecutingPath).parentPath ++ "test.txt";
f = File(path.standardizePath,"w");
f.write(~const_MILL_OFFSET.asString);
f.close;
)

File.readAllString(PathName(thisProcess.nowExecutingPath).parentPath ++ "test.txt").asFloat;



o = OSCFunc({ |msg| ~const_MILL_OFFSET = msg[3]; }, '/set_mill_offset');
o.free


~setMillZeroPosition = S


(
~rrrr = Routine.new({
	inf.do{
	~bus_MILL_RAW_IN.get({arg val;
			if(val>=~max, {~max = val;});
			if(val<~min, {~min = val;});
			//val.postln;
		});
	wait(0.05);
	};
}).play;
)
~rrrr.free;

~test.free
s.freeAll
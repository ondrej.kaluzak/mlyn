
(
//DEFINE OSC ANND KEY
// NetAddr.langPort
//~osc = NetAddr.new("192.168.1.105", 57120);
//~osc = NetAddr.new("127.0.0.1", 57120);
~osc = NetAddr.new("127.0.0.1", 7400); // to MAX
//~const_OSC_SK = 'test';

~const_OSC_MILL_INFO = '/mlyn-osc-simulator-key';

~oscDef_Mill_Listener = OSCdef(\mlyn_osc_mill_listener, {|msg|
	var phase = msg[1];
	~bus_MILL_RAW_IN.set(phase.linlin(0, 1, 0, 1));
}, ~const_OSC_MILL_INFO);

///////////////////////////////////

~const_OSC_CAP_INFO = '/mlyn-osc-simulator-cap-key';

~oscDef_Cap_Listener = OSCdef(\mlyn_osc_cap_listener, {|msg|
	var data = msg[1];
	~bus_CAP_RAW.set(data);

}, ~const_OSC_CAP_INFO);

////////////////////////////////////

~const_OSC_MOVEMENT_INFO = '/mlyn-osc-simulator-movement-key';

~oscDef_Cap_Listener = OSCdef(\mlyn_osc_movement_listener, {|msg|
	var data = msg[1];
	~bus_INFRA_RAW_IN.set(data);

}, ~const_OSC_MOVEMENT_INFO);

)
(
var infoFilesRootPath = "../audio/src/info/";
var folderPaths = PathName(infoFilesRootPath.resolveRelative).folders;

~dict_InfoSoundsDefinitions = ();

// inicializuj strukturovanu db
/*

!!INFO FILE FORMAT!!

[nalada]-[aktivacia][lubovolny string popisujuci subor].[pripona]

neutral-overload-
neutral-normal-
neutral-arousal-


sad-overload-
sad-normal-
sad-arousal-

happy-overload-
happy-normal-
happy-arousal-

*/
"CREATING INFO DATABASE:".postln;

folderPaths.do{
	|path|
	var name = path.folderName;

	~dict_InfoSoundsDefinitions[name.asSymbol] = ();
	
	[\neutral, \sad, \happy].do{
		|mood|
		var moodPaths = path.entries.select{|item| item.fileName.beginsWith(mood.asString)};
		~dict_InfoSoundsDefinitions[name.asSymbol][mood] = ();
		[\normal, \overload, \arousal, \excited].do({
			|activation|
			var activationFiles = moodPaths.select{
				|path|
				path.fileName.beginsWith(mood.asString ++ "-" ++ activation.asString)
			}.collect{ |item| item.fullPath };
			~dict_InfoSoundsDefinitions[name.asSymbol][mood][activation] = activationFiles;

			if(~dict_InfoSoundsDefinitions[name.asSymbol][mood][activation].size == 0, {
				~dict_InfoSoundsDefinitions[name.asSymbol][mood][activation] = ~dict_InfoSoundsDefinitions[name.asSymbol][mood][\normal]
			});

		});

	};
};

~fn_GetInfoFilePath = {
	arg name = \empty;
	var mood, activation, path;
	mood = ~dict_Soul.mood.state;
	activation = ~dict_Soul.activation.state;
	path = ~dict_InfoSoundsDefinitions[name][mood][activation].choose;
	path.postln;
	if(path.isNil, {
		path = ~dict_InfoSoundsDefinitions[name][\neutral][activation].choose;
	});
	path;
};

~fn_GetInfoFilePath_OLD_TO_DELETE = {
	arg name = \empty;
	var mood = ~dict_Soul.mood.state;
	var filename = ~dict_InfoSoundsDefinitions[name].variant[mood].choose;
	var defaultVariant = ~dict_InfoSoundsDefinitions[name].variant[\default];
	if(filename.isNil, {
		filename = ~dict_InfoSoundsDefinitions[name].variant[defaultVariant].choose;
		PathName(infoFilesRootPath.resolveRelative +/+ defaultVariant +/+ filename).fullPath;
	}, {
		PathName(infoFilesRootPath.resolveRelative +/+ mood +/+ filename).fullPath;
	});
};

)

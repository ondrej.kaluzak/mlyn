//mlynek - idle.scd

(
// IDLE STATE
var env = ~e.states[\idle];
var envRand = ~e.utils.envRand;

var cap_blocking_time = env.blocking_time_cap_after_start;
var mill_blocking_time = env.blocking_time_mill_after_start;

var shuffledPauses = env.trigger_do_something_interval_shuffled_pauses;

shuffledPauses = shuffledPauses.scramble;

~var_MILL_MOVING = false;
~var_CAP_OPENED = false;
~var_MOVEMENT = false;
~var_NEW_MOVEMENT = false;

~r_IdleRoutine = Routine.new({
	var i = 0;
	var waitTime = 0.05;
	var showLabelMod = (1 / waitTime) * 10;
	var questionModFreq = 50.rand + 10;
	var selectNewBuffsModFreq = 50.rand + 10;
	var idleSynth;

	var minDoSomethingTime = envRand.(*env.trigger_do_something_interval);
	var minNewMovementTime = envRand.(*env.trigger_new_movement_interval);

	minDoSomethingTime = minDoSomethingTime + shuffledPauses[0];
	shuffledPauses = shuffledPauses.rotate;

	postln("minDoSomethingTime = " + minDoSomethingTime);
	postln("minNewMovementTime = " + minNewMovementTime);

	~var_IDLE_TIME = 0;

	0.01.wait;
	"\nEntering IDLE routine".postln;
	~var_IRC[\no_idle] = false;
	/*if(~r_IdleAnalogSynth.isPlaying.not, {
		~r_IdleAnalogSynth = fork{
			inf.do{
				var dur = rrand(50, 120);
				~fn_AnalogSynth_IdleSeq.(
					mood: ~dict_Soul.mood.state,
					dur: dur,
					onFinish: {~r_IdleAnalogSynth.stop;}
				);
				wait(dur * 1.5);
			};
		};
	});
*/


	inf.do{
		var sinceStart = ~fn_UtilClockGetSince.(\system_started);
		if(i.mod(showLabelMod.floor) == 0, {postln("IDLE (" + i + ")")});
		i=(i+1).mod(50000);

		~var_IDLE_TIME = i / (1/waitTime);


		if(~var_MILL_MOVING && (sinceStart > mill_blocking_time), {
			~r_IdleAnalogSynth.stop;
			~var_IRC[\no_idle] = true;
			~fn_requestStateChange.(\mill);
			~r_IdleRoutine.stop;

		});

		if(~var_CAP_OPENED && (sinceStart > cap_blocking_time), {
			~r_IdleAnalogSynth.stop;
			~var_IRC[\no_idle] = true;
			~fn_requestStateChange.(\rec);
			~r_IdleRoutine.stop;
		});

		if(~var_NEW_MOVEMENT && (~fn_StatsSince.(\greeting) > minNewMovementTime), {
			~r_IdleAnalogSynth.stop;
			~var_IRC[\no_idle] = true;
			~fn_requestStateChange.(\new_movement);
			~r_IdleRoutine.stop;
		});

		if((~var_IDLE_TIME > 20) && (~fn_StatsSince.(\rec) > rrand(6, 8)), {
			~r_IdleAnalogSynth.stop;
			~var_IRC[\no_idle] = true;
			~fn_requestStateChange.(\select_new_buffers);
			~r_IdleRoutine.stop;
		});

		if((~var_IDLE_TIME > 1000000000) && (~fn_StatsSince.(\doSomething) > minDoSomethingTime), {
			~r_IdleAnalogSynth.stop;
			~var_IRC[\no_idle] = true;
			~fn_requestStateChange.(\do_something);
			~r_IdleRoutine.stop;
		});


		waitTime.wait;
	}
});
~fn_setPatchLoaded.value(~fn_getPatchName.value);
)

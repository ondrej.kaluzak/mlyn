(
//////////SERVER/////////////
Server.default = Server.local;
s.options.blockSize = 256;
s.options.memSize = 2.pow(16);
s.boot;
//s.connect
/////////////////////////////
)
s.quit
Server.remote

///////////////////////////////////////////////
///////////////////////////////////////////////
///////////////////START HERE /////////////////
///////////////////////////////////////////////
///////////////////////////////////////////////
(
var projectFolder, mainPath;
var calibrationPath;
var configPath;
var calibrationConfigDict;

s.freeAll;
OSCdef.freeAll;
Routine.clear;
Buffer.freeAll;
s.freeAllBuffers;
currentEnvironment.clear;

projectFolder = PathName(thisProcess.nowExecutingPath).parentPath;
mainPath = projectFolder ++ './main.scd';


calibrationPath = projectFolder ++ 'calibration.scd';
configPath = projectFolder ++ 'calibration-config.scd';
calibrationConfigDict = this.executeFile(configPath);

~env = 'pc-local';
~const_DEBUG_LOAD_ENV_ONLY = true;
~const_DEBUG_OSC_TO_MAX = false;

~fn_DEBUG_AFTER_LOAD = {
	"\n\n************\n**DEBUG FN**\n************\n\n".postln;

/*
	~fn_DecideAction.(overrideAction: \user_and_question_chaotic, onFinish: {"onFinish".postln;});
	~fn_DecideAction.(overrideAction: \user, onFinish: {"onFinish".postln;});
	~fn_DecideAction.(overrideAction: \question_chaotic, onFinish: {"onFinish".postln;});
*/
	// Synth(\vocoPhasePlayer, [
	// 	out: ~bus_MasterOut,
	// 	busMillRps: ~bus_MILL_RPS,
	// 	busMillRaw: ~bus_MILL_RAW_IN,
	// 	\busMillMovingCountdown, ~bus_MILL_MOVING_COUNTDOWN,
	// 	gate: 1,
	// 	fftBuf: ~buf_FFT_LastRecBuf.bufnum,
	// ]);
	/*
	~e.utils.envRand.(*~e.states[\idle].trigger_do_something_interval);
	(
	var minDoSomethingTime = ~e.utils.envRand.(*~e.states[\idle].trigger_do_something_interval);
	postln("minDoSomethingTime = " + minDoSomethingTime);
	)

	postln("minDoSomethingTime = " + 50);
*/
	~fn_GetIndexedQuestions.().keys.postln;

	~dict_recDatabase.items.size;
	~var_CURRENT_QUESTION;
	~selectedUserBufs;

	//~playInfo.(\rec_start);
	//fork { 5.do{|i| (\sustain: 0.1, \freq: ((i+4)*12).midicps, \amp: 1).play; 0.1.wait; }};
	/*
	p = Pmono(
		\default,
		\degree, Pseq([1,2,3], 4)
	).play;

	p.stop
	*/
	//~fn_GetIndexedQuestions.()['q-cgrosso.wav'];
/*
	~fn_LoadQuestionByName.(fileName: 'q-stojan.wav',
		onLoadFinished: {arg cq;
			cq.postln;
			b = cq.buf;
			Synth(\playBuffer, [
				\buf, cq.buf,
				\rate, 1,
				\amp, 1,
				\out, 0
			]);
	});
*/
/*
	~fn_LoadNextQuestionSync.(
		onLoadFinished: {arg cq;
			cq.postln;
			b = cq.buf;
			c.free;
			c = Synth(\playBuffer, [
				\buf, cq.buf,
				\rate, 1,
				\amp, 1,
				\out, 0
			]);
	});
	*/
	"\n\n************\n**END DEBUG FN**\n************\n\n".postln;
};

~const_DEBUG_SOUL_MANUAL_OVERRIDE = true;
~const_DEBUG_SOUL_MOOD = \sad;
~const_DEBUG_SOUL_ACTIVATION = \arousal;


~bus_MILL_RAW_IN = Bus.control(s, 1);
~bus_CAP_RAW = Bus.control(s, 1);
~bus_INFRA_RAW_IN = Bus.control(s, 1);
~bus_LED_IN = Bus.control(s, 3); // RGB
~const_MicIn_GAIN = 0.5;

//mainPath.load;
calibrationPath.load;

fork {
	1.wait;
	~fn_LoadSimulator.();

	~osc = NetAddr.new("127.0.0.1", 7400); // to MAX
	"OSC TO MAX INITIALIZED".postln;
	~oscFunc = {
		~osc.sendMsg(\mood, ~dict_Soul.mood.state);
		~osc.sendMsg(\mood_counter, ~dict_Soul.mood.counter);

		~osc.sendMsg(\activation, ~dict_Soul.activation.state);
		~osc.sendMsg(\activation_counter, ~dict_Soul.activation.counter);

		~osc.sendMsg(\st_stats_cap_since , ~fn_StatsSince.(\rec));
		~osc.sendMsg(\st_stats_cap , ~dict_ST_Stats.rec.inLastMinuteCount);
		~osc.sendMsg(\st_stats_cap_failed , ~fn_LT_Stats_GetRecentFailedRecCount.(time: 60));

	};
	if(~const_DEBUG_OSC_TO_MAX == true, {
		inf.do{
			try{
				~oscFunc.();
			};
			0.05.wait;
		};
	});
};


)
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////
(
~oscFunc = {
	~osc.sendMsg(\mood, ~dict_Soul.mood.state);
	~osc.sendMsg(\mood_counter, ~dict_Soul.mood.counter);

	~osc.sendMsg(\activation, ~dict_Soul.activation.state);
	~osc.sendMsg(\activation_counter, ~dict_Soul.activation.counter);

	~osc.sendMsg(\st_stats_cap_since , ~fn_StatsSince.(\rec));
	~osc.sendMsg(\st_stats_cap , ~dict_ST_Stats.rec.inLastMinuteCount);

};
)



(
Synth(\playBuffer, [
	\buf, b,
	\rate, 0.7,
	\amp, 1,
	//\out, [0,1],
]);
)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////// SHELL COMMANDS /////////////////
p = Pipe.new("ls -l", "r");
p.getLine

Platform.case(
	\osx,       { "OSX".postln },
	\linux,     { "Linux".postln },
	\windows,   { "Windows".postln }
);

(
var path = PathName(thisProcess.nowExecutingPath).parentPath;
var p, x;
path.postln;
p = Pipe.new("D: & cd " ++ path ++ " & dir", "r");            // list directory contents in long format

x = p.getLine;                    // get the first line
while({x.notNil}, {x.postln; x = p.getLine; });    // post until l = nil
p.close;                    // close the pipe to avoid that nasty buildup
)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

~fn_SoulUpdateActivation.(15);
~dict_Soul.activation.state = \normal

((1..100).lincurve(0,100,1,8,4)-1).plot;
~dict_Soul.activation
~var_STATE
~var_STATE_REQUEST
~dict_LT_Stats
~fn_StatsSince.(\movement)
~fn_StatsSince.(\newMovement)
~fn_StatsSince.(\greeting)
~dict_ST_Stats.newMovement
DelayC
s.freeAll;
Server.killAll;
s.quit
Window.closeAll;
//////////
(
s.freeAll;
OSCdef.freeAll;
Routine.clear;
Buffer.freeAll;
s.freeAllBuffers;
currentEnvironment.clear;
)
////////////
s.reboot

MethodOverride.printAll
View
Routine.inspect;
s.free
//p = s.plotTreeView(0.5, View(bounds: Point(0,0))).();

if

~var_IRC.keys.do{|k| ~var_IRC[k].postln}
~var_STATE_PREVIOUS
~fn_LT_PrintRecords.(\mill)
~fn_LT_PrintRecords.(\doSomething)
~fn_LT_PrintRecords.(\newMovement)

~dict_Soul.stats.mill.counter
~dict_Soul.stats[\doSomething].last = Date.getDate.rawSeconds;
~dict_Soul.stats[\doSomething].count = ~dict_Soul.stats[\doSomething].count + 1;
~fn_StatsUpdate.(name: \doSomething);
~fn_MoodUpdate.(1)

~dict_Soul.verboseStatistics

~dict_recDatabase.items.size
~dict_recDatabase.items[0]
(0..(~dict_recDatabase.items.size-1).max(0)).scramble;
(0..5.max(0)).scramble;


0.00165432.floor(0.001)
0.00295432.trunc(0.001)

x = [1,2,3];
x = x.rotate
Date.getDate().stamp
(
{
	10.clip(1,5)
}.play
)
~var_IRC
~var_Interupt = false;
~var_Interupt = true;

s.meter

~buff_LastRecBuff.numFrames / s.sampleRate
~buff_LastRecBuff.play
~buff_LastRecBuff.plot;
~buff_MainBuf.numFrames / s.sampleRate
~buff_MainBuf.play
~buff_MainBuf.plot

~var_CURRENT_STATE.postln;



///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

((0..1000)/1000).lincurve(0,1,0.01,1, -32).plot;

(
x = {
	arg a, b, c;
	a.postln;
	b.postln;
	c.postln;
	"konec";
}
)
(
y = {
	arg f = #[0,0,0];
	x.(*f);
}
)
y.([1,2,3])

(
SynthDef(\analog, {
	var sig = 0;
	var freq = \freq.kr(440);
	var amp = \amp.kr(0.5);
	sig = VarSaw.ar(freq, 0.0, 0.0);
	sig = sig * Env.adsr(\atk.kr(0.1), \decay.kr(0.1), \slev.kr(0.5), \rel.kr(0.1)).ar(gate: \gate.kr(1), doneAction: 2);
	sig = sig * amp;
	Out.ar(0, sig!2);

}).add;

)

(
p = Pbind(
	\instrument, \analog,
	\sustain, 0.01,
	\atk, Pbrown(0.001, 0.01, 0.001, inf),
	\dur, Pwhite(0.1, 0.5, inf),
	\slev, Pwhite(0.001, 0.1, inf),
	\decay, Pwhite(0.001, 0.01, inf),
	\rel, Pbrown(0.01, 0.08, 0.1, inf),
	\amp, Pseq([0.5, 0.3, Pwhite(0.001, 0.1, 1)], inf),
	\freq, Pbrown(500, 1000,100, inf),
);

q = Pbind(
	\instrument, \analog,
	\sustain, 0.1,
	\atk, 0.001,
	\dur, Pbrown(0.1, 1, 0.01, inf),
	\slev, 0,
	\decay, 0.001,
	\rel, 0.001,
	\amp, 0.3,
	\freq, Pwhite(1000, 2000, inf),

);

Ppar([p, q], inf).play;
)
///////////////////////////
(
r = PmonoArtic(

	\analog,
	\sustain, 1,
	\atk, 0.1,
	\dur, Pseq([1, 2, 3], inf),
	\slev, 0.5,
	\decay, 0.1,
	\rel, 0.5,
	\amp, 0.1,
	\scale, Scale.majorPentatonic,
	\degree, Pbrown(0, 12, 1, inf),
	\octave, 2,
	\legato, Pseq([1, 1, 1, 0.2, 0.5, 1, 0.1, 0.4, 1], inf),
);

Ppar([r], inf).play;
)

///////////////////////////
(
q = Pbind(
	\instrument, \analog,
	\sustain, 0.01,
	\atk, 0.001,
	\dur, Pseq([Pwhite(0.5, 0.7, 1), 0.3,0.1, Pn(0.2, 5)], inf),
	\slev, Pseq([Pn(0.01, 9), 0.1], inf),
	\decay, 0.01,
	\rel, Pseq([Pn(0.01, 10), 1.2], inf),
	\amp, 0.3,
	\freq, Pseq([Prand([600, 625, 650], 1), 450, Pn(Pwhite(300, 350, 1), 5)], inf),

);

Ppar([q], inf).play;


)
import fs from "fs";
import { zipDirectory, unzipDirectory } from "./zip.js";
import { initializeApp } from "firebase/app";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { firebaseConfig } from "./firebase.config.js";
import { getStorage, ref, uploadBytes, getBytes } from "firebase/storage";
import readline from "readline";

export const loadJSON = (path) => {
  let result = undefined;
  try {
    result = JSON.parse(fs.readFileSync(path));
    return result;
  } catch (error) {
    console.log("creds not found.");
    return undefined;
  }
};

export const uploadFileToFirebase = async ({
  firebasePath,
  localFilePath,
  credentials,
}) => {
  initializeApp(firebaseConfig);
  const auth = getAuth();
  await signInWithEmailAndPassword(
    auth,
    credentials.username,
    credentials.password
  );
  const storage = getStorage();
  const file = fs.readFileSync(localFilePath);
  const storageRef = ref(storage, firebasePath);
  console.log("Uploading to:", firebasePath);
  await uploadBytes(storageRef, file);
  console.log("Finished.");
};

export const createZipAndUpload = async ({
  folderPath,
  firebasePath,
  credentials,
}) => {
  const zipFilePath = `./temp.zip`;
  await zipDirectory(folderPath, zipFilePath);
  await uploadFileToFirebase({
    firebasePath,
    localFilePath: zipFilePath,
    credentials,
  });
  console.log("Removing local zip file...", zipFilePath);
  fs.rmSync(zipFilePath);
  console.log("Finished.");
};

export const downloadFilesFromFirebase = async ({
  firebasePath,
  outputPath,
  credentials,
}) => {
  initializeApp(firebaseConfig);
  const auth = getAuth();
  await signInWithEmailAndPassword(
    auth,
    credentials.username,
    credentials.password
  );
  const storage = getStorage();
  const storageRef = ref(storage, firebasePath);
  console.log("Downloading from firebase: ", firebasePath, "to", outputPath);
  const arrayBuffer = await getBytes(storageRef);
  fs.appendFileSync(outputPath, Buffer.from(arrayBuffer));
  console.log("Finished.");
};

export const downloadFileAndUnzip = async ({
  firebasePath,
  outputPath,
  credentials,
}) => {
  const tempFilePath = "./temp.zip";
  await downloadFilesFromFirebase({
    firebasePath,
    outputPath: tempFilePath,
    credentials,
  });
  console.log("Unzipping file...", tempFilePath);
  await unzipDirectory(tempFilePath, outputPath);
  console.log("Finished.");
  console.log("Removing local zip file...", tempFilePath);
  fs.rmSync(tempFilePath);
  console.log("Finished.");
};

export const checkCredentials = (credentials) => {
  if (!credentials.username || !credentials.password) {
    console.error("Missing ", paths.local.credentials);
    return false;
  } else {
    console.log(
      `credentials = ${credentials.username}:${credentials.password}`
    );
    return true;
  }
};

export const createCredentials = (pathToCredentials) => {
  const input = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  let username = "";
  let password = "";

  input.question("Username: ", (usernameInput) => {
    username = usernameInput;

    input.question("Password: ", (passwordInput) => {
      password = passwordInput;
      try {
        fs.writeFileSync(
          pathToCredentials,
          JSON.stringify({ username, password })
        );
      } catch (error) {
        console.log(error);
      }
      input.close();
      console.log("Creds initialized");
    });
  });
};

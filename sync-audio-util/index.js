import {
  loadJSON,
  checkCredentials,
  createCredentials,
  downloadFileAndUnzip,
  createZipAndUpload,
} from "./utils.js";

const ACTION = process.env["ACTION"];
const version = process.argv[2] ?? 'default';
const Variants = {
  src: "src",
  usr: "usr",
  questions: "questions",
  credentials: "credentials",
};

const paths = {
  firebase: {
    [Variants.src]: "mlyn/audio/src",
    [Variants.questions]: "mlyn/audio/src/questions",
    [Variants.usr]: "mlyn/audio/users",
  },
  local: {
    [Variants.src]: "../sc/audio/src",
    [Variants.usr]: "../sc/audio/users",
    [Variants.credentials]: "./credentials.json",
    [Variants.questions]: "../sc/audio/src/questions"
  },
};

const filenames = {
  [Variants.src]: `src-version-${version}.zip`,
  [Variants.usr]: `users-version-${version}.zip`,
  [Variants.questions]: `questions-version-${version}.zip`,
}

const getFilenameTemplate = (variant) => filenames[variant]


const credentials = loadJSON(paths.local.credentials);

///////////////////////// UPLOAD ///////////////////////////

if (ACTION === "UPLOAD") {
  if (checkCredentials(credentials)) {
    const filename = getFilenameTemplate(Variants.src);
    createZipAndUpload({
      folderPath: paths.local.src,
      firebasePath: `${paths.firebase.src}/${filename}`,
      credentials,
    });
  }
}

if (ACTION === "UPLOAD_QUESTIONS") {
  if (checkCredentials(credentials)) {
    const variant = Variants.questions;
    const filename = getFilenameTemplate(variant);
    createZipAndUpload({
      folderPath: paths.local[variant],
      firebasePath: `${paths.firebase[variant]}/${filename}`,
      credentials,
    });
  }
}

if (ACTION === "UPLOAD_USERS") {
  if (checkCredentials(credentials)) {
    const variant = Variants.usr;
    const filename = getFilenameTemplate(variant);
    createZipAndUpload({
      folderPath: paths.local[variant],
      firebasePath: `${paths.firebase[variant]}/${filename}`,
      credentials,
    });
  }
}
/////////////////////////////////////////////////////////

///////////////////////// DOWNLOAD /////////////////////////
if (ACTION === "DOWNLOAD") {
  if (checkCredentials(credentials)) {
    const variant = Variants.src;
    const filename = getFilenameTemplate(variant);
    downloadFileAndUnzip({
      firebasePath: `${paths.firebase[variant]}/${filename}`,
      outputPath: paths.local[variant],
      credentials,
    });
  }
}

if (ACTION === "DOWNLOAD_USERS") {
  if (checkCredentials(credentials)) {
    const variant = Variants.usr;
    const filename = getFilenameTemplate(variant);
    downloadFileAndUnzip({
      firebasePath: `${paths.firebase[variant]}/${filename}`,
      outputPath: paths.local[variant],
      credentials,
    });
  }
}

if (ACTION === "DOWNLOAD_QUESTIONS") {
  if (checkCredentials(credentials)) {
    const variant = Variants.questions;
    const filename = getFilenameTemplate(variant);
    downloadFileAndUnzip({
      firebasePath: `${paths.firebase[variant]}/${filename}`,
      outputPath: paths.local[variant],
      credentials,
    });
  }
}
/////////////////////////////////////////////////////////

/////////////////////CREDENTIALS/////////////////////////
if (ACTION === "INIT_CREDS") {
  const variant = Variants.credentials;
  createCredentials(paths.local[variant]);
}
/////////////////////////////////////////////////////////

if (ACTION === "TEST") {
  console.log("Test is currently empty.");
}

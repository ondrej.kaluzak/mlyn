import AdmZip from 'adm-zip'

/**
 * 
 * @param {*} sourceDir string - source directory
 * @param {*} outputFilePath string - destination file path
 */
export const zipDirectory = async (sourceDir, outputFilePath) => {
    console.log("Zipping...");
    const zip = new AdmZip();
    zip.addLocalFolder(sourceDir);
    await zip.writeZipPromise(outputFilePath);
    console.log(`Zip file created: ${outputFilePath}`);
};

/**
 * 
 * @param {*} inputFilePath 
 * @param {*} outputDirectory 
 * @returns 
 */
export const unzipDirectory = async (inputFilePath, outputDirectory) => {
    const zip = new AdmZip(inputFilePath);
    zip.extractAllTo(outputDirectory, true);
    // for async version
    /*return new Promise((resolve, reject) => {
        zip.extractAllToAsync(outputDirectory, true, (error) => {
            if (error) {
                console.log("Error");
                console.log(error);
                reject(error);
            } else {
                console.log(`Extracted to "${outputDirectory}" successfully`);
                resolve();
            }
        });
    });*/
};